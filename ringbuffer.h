#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct ringbuffer_instance_t* ringbuffer_t;

ringbuffer_t ringbuffer_create(uint32_t capacity);
uint32_t ringbuffer_capacity(ringbuffer_t instance);
uint32_t ringbuffer_used(ringbuffer_t instance);
bool ringbuffer_enqueue(ringbuffer_t instance, uint8_t item);
bool ringbuffer_dequeue(ringbuffer_t instance, uint8_t* item);
void ringbuffer_destroy(ringbuffer_t instance);
void ringbuffer_debug(ringbuffer_t instance);
bool ringbuffer_empty(ringbuffer_t instance);
bool ringbuffer_full(ringbuffer_t instance);
