#include "ringbuffer.h"
#include <assert.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	ringbuffer_t inst = ringbuffer_create(4);
	ringbuffer_debug(inst);
	uint8_t item;
	assert(ringbuffer_enqueue(inst, 1));
	ringbuffer_debug(inst);
	assert(ringbuffer_enqueue(inst, 2));
	ringbuffer_debug(inst);
	assert(ringbuffer_enqueue(inst, 3));
	ringbuffer_debug(inst);
	assert(ringbuffer_enqueue(inst, 4));
	ringbuffer_debug(inst);
	assert(ringbuffer_enqueue(inst, 5) == false);
	ringbuffer_debug(inst);
	assert(ringbuffer_dequeue(inst, &item));
	assert(item == 1);
	ringbuffer_debug(inst);
	assert(ringbuffer_enqueue(inst, 5));
	ringbuffer_debug(inst);

	assert(ringbuffer_dequeue(inst, &item));
	assert(item == 2);
	ringbuffer_debug(inst);

	assert(ringbuffer_dequeue(inst, &item));
	assert(item == 3);
	ringbuffer_debug(inst);

	assert(ringbuffer_dequeue(inst, &item));
	assert(item == 4);
	ringbuffer_debug(inst);

	assert(ringbuffer_dequeue(inst, &item));
	assert(item == 5);
	ringbuffer_debug(inst);

	assert(ringbuffer_dequeue(inst, &item) == false);
	ringbuffer_debug(inst);
	return 0;
}
