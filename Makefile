# Preproc options
# -MMD is used to generate .d files for user header dependencies (use -MD for system and user header instead)
CPPFLAGS = -MMD
# main compilation
CFLAGS ?= -Werror -Wall #$(shell pkg-config --cflags sdl)
# C++ flags
CXXFLAGS =
#Linker flags
LDFLAGS =
#Linker path
LDLIBS = #$(shell pkg-config --libs sdl)

ifneq ($(CROSS_COMPILE),)
	CC  :=$(CROSS_COMPILE)$(CC)
	CXX :=$(CROSS_COMPILE)$(CXX)
	LD  :=$(CROSS_COMPILE)$(LD)
endif

bin = main
lib = libringbuffer.so

#Will be compiled with -fpic
lib_src = ringbuffer.c
lib_obj = $(lib_src:%.c=%.o)
lib_dep = $(lib_src:%.c=%.d)

sources = $(filter-out $(lib_src), $(wildcard *.c))
objects = $(sources:%.c=%.o)
depends = $(sources:%.c=%.d)

all: $(bin) $(lib)

$(bin): $(objects) $(lib)


$(lib): CFLAGS += -fpic
$(lib): $(lib_obj)

%.so:
	$(LINK.c) -shared $^ $(LDLIBS) -o $@

.PHONY: clean
clean:
	$(RM) $(bin) $(objects) $(depends) $(lib) $(lib_obj) $(lib_dep)

ifneq ($(MAKECMDGOALS),clean)
-include $(depends)
-include $(lib_dep)
endif
